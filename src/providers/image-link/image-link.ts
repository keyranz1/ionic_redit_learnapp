import {Injectable} from '@angular/core';

@Injectable()
export class ImageLinkProvider {

  constructor() {
  }

  getImage(imageUrl: string, defaultFallbackLinkType?: string): string {

    if (imageUrl && imageUrl.includes('https')) {
      return imageUrl;
    }

    if (defaultFallbackLinkType === 'DETAILS_PAGE') {
      return 'https://upload.wikimedia.org/wikipedia/en/thumb/8/82/Reddit_logo_and_wordmark.svg/1200px-Reddit_logo_and_wordmark.svg.png';
    }

    return 'https://www.novelupdates.com/img/noimagefound.jpg';
  }

}
