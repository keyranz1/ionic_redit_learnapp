import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable";


@Injectable()
export class RestProvider {
  http: any;
  baseUrl: string;

  constructor(http: HttpClient) {
    this.http = http;
    this.baseUrl = 'https://www.reddit.com/r';
  }

  getPosts(category, limit): Observable<any> {
    return this.http.get(this.baseUrl + '/' + category + '/top.json?limit=' + limit);
  }

}
