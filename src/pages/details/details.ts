import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {ImageLinkProvider} from "../../providers/image-link/image-link";

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-details',
  templateUrl: 'details.html'
})
export class DetailsPage {
  item:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public imageLink : ImageLinkProvider) {
    this.item = navParams.get('item');
  }

}
