import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {RedditPage} from "../reddit/reddit";

const categories: string[] = ['movies','food','art','funny','news','gaming','sports','music'];
const limits: number[] = [5,10,15,20,25,30,35,40,45,50,60,70,80,90,100];

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  redditCategories: string[] = categories;
  redditLimits: number[] = limits;
  selectedCategory: string;
  selectedLimit: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initializeProperties();
  }

  initializeProperties(): void {
    if(localStorage.getItem('selectedCategory') !== null){
      this.selectedCategory = localStorage.getItem('selectedCategory');
    }else {
      this.selectedCategory = this.redditCategories[0];
    }
    if(localStorage.getItem('selectedLimit') !== null){
      this.selectedLimit = parseInt(localStorage.getItem('selectedLimit'));
    }else {
      this.selectedLimit = this.redditLimits[1];
    }
  }

  getDefaults():void{
    if(localStorage.getItem('selectedCategory') !== null){
        this.selectedCategory = localStorage.getItem('selectedCategory');
    }
  }
  setDefaults(){
    localStorage.setItem('selectedCategory',this.selectedCategory);
    localStorage.setItem('selectedLimit',this.selectedLimit+'');
    this.navCtrl.push(RedditPage);
  }

}
