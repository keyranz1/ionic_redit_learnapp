import {Component} from '@angular/core';
import { NavController} from 'ionic-angular';
import {RestProvider} from "../../providers/rest/rest";
import {DetailsPage} from "../details/details";
import {ImageLinkProvider} from "../../providers/image-link/image-link";


@Component({
  selector: 'page-reddit',
  templateUrl: 'reddit.html',
})
export class RedditPage {
  items: any[];
  category: string;
  limit: number;

  constructor(public navCtrl: NavController, private restProvider: RestProvider, public imageLink: ImageLinkProvider) {
    this.initializeProperties();
  }

  //lifecycle hook
  ngOnInit() {
    this.getPost('movies', 25);
  }

  initializeProperties(): void {
    if(localStorage.getItem('selectedCategory') !== null){
      this.category = localStorage.getItem('selectedCategory');
    }else {
      this.category = 'movies';
    }
    if(localStorage.getItem('selectedLimit') !== null){
      this.limit = parseInt(localStorage.getItem('selectedLimit'));
    }else {
      this.limit = 10;
    }
  }

  getPost(category, limit) {
    this.restProvider.getPosts(category, limit)
      .toPromise()
      .then((response: any) => {
        console.log(response);
        this.items = response.data.children;
      });
  }

  viewItem(item) {
    this.navCtrl.push(DetailsPage, {
      item: item
    });
  }

  changeCategory() {
    this.getPost(this.category, this.limit);
  }
}
